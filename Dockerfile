FROM ubuntu:trusty
RUN apt-get update -yqq \
    && apt-get install -yqq \
        php5 \
        php5-mcrypt \
        php-pear \
        php5-dev \
        git \
        php5-apcu \
        php5-gd \
        php5-mysql \
        php5-ldap \
        php5-curl \
        php5-mssql \
        libssh2-1-dev \
        libssh2-php \
        imagemagick \
        php5-imagick \
        php5-xsl \
        php5-intl \
        curl \
        sendmail \
    && pecl install scrypt \
    && echo "extension=scrypt.so" \
        > /etc/php5/mods-available/scrypt.ini \
    && php5enmod scrypt \
    && php5enmod ssh2 \
    && curl -sS https://getcomposer.org/installer | php